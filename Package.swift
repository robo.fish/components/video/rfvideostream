// swift-tools-version:5.3

import PackageDescription

let package = Package(
	name: "RFVideoStream",
	platforms: [.iOS(.v13)],
	products: [
		.library(
			name: "RFVideoStream",
			targets: ["RFVideoStream"]),
	],
	dependencies: [
	],
	targets: [
		.target(
			name: "RFVideoStream",
			dependencies: [])
	]
)
