# RFVideoStream

Video stream abstraction with source, sink, and processor elements, based on iOS AVCapture API, designed for use in SwiftUI.
