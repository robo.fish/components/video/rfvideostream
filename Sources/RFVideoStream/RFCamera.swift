//  Copyright 2020 Robo.Fish UG

import AVFoundation

public class RFCamera
{
	public enum CameraType
	{
		case backWide, backUltraWide, backTele, frontWide, frontDepth

		var isFrontCamera : Bool { return self == .frontDepth || self == .frontWide }
	}

	private let _deviceInput : AVCaptureDeviceInput

	public init?(cameraType : CameraType = .backWide)
	{
		guard let camera = Self._device(for: cameraType) else { return nil }
		guard let deviceInput = try? AVCaptureDeviceInput(device: camera) else { return nil }
		_deviceInput = deviceInput
	}

	private static func _device(for type : CameraType) -> AVCaptureDevice?
	{
		let deviceType : AVCaptureDevice.DeviceType = {
			switch type {
				case .frontWide : return .builtInWideAngleCamera
				case .frontDepth : return .builtInTrueDepthCamera
				case .backWide : return .builtInWideAngleCamera
				case .backUltraWide : return .builtInUltraWideCamera
				case .backTele : return .builtInTelephotoCamera
			}
		}()
		return AVCaptureDevice.DiscoverySession(deviceTypes: [deviceType], mediaType: .video, position: type.isFrontCamera ? .front : .back).devices.first
	}

}

extension RFCamera : RFVideoSource
{
	public var videoInput : AVCaptureInput
	{
		_deviceInput
	}
}
