//  Copyright 2020 Robo.Fish UG

import AVFoundation

public protocol RFVideoSource
{
	var videoInput : AVCaptureInput { get }
}
