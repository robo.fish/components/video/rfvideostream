//  Copyright 2020 Robo.Fish UG

import CoreVideo

public protocol RFVideoProcessor
{
	@discardableResult
	func process(_ : CVPixelBuffer) -> Bool
}
