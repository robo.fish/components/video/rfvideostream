//  Copyright 2020 Robo.Fish UG

import UIKit
import AVFoundation

public protocol RFVideoSink : RFVideoProcessor
{
	var videoSession : AVCaptureSession? { get set }

	var view : UIView? { get }
}
