//  Copyright 2020 Robo.Fish UG

import AVFoundation
import UIKit
import Combine

public class RFVideoStream : NSObject
{
	public typealias RFVideoSourceObject = AnyObject & RFVideoSource
	public typealias RFVideoSinkObject = AnyObject & RFVideoSink
	public typealias RFVideoFilterObject = AnyObject & RFVideoFilter

	private let _captureSession = AVCaptureSession()
	private let _videoOutput = AVCaptureVideoDataOutput()
	private let _sessionQueue = DispatchQueue(label: "RFVideoStream", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
	private var _lastCapturedImage : CVPixelBuffer?
	private var _observer: NSKeyValueObservation?
	private var _cancellableSubscriptions = [AnyCancellable]()
	private var _sink : RFVideoSinkObject?
	private var _filters : [RFVideoFilterObject] = []

	let source : RFVideoSourceObject

	public var filters : [RFVideoFilterObject]
	{
		get { _filters }
		set
		{
			_sessionQueue.async {
				self._captureSession.stopRunning()
				self._filters = newValue
				self._captureSession.startRunning()
			}
		}
	}

	public var sink : RFVideoSinkObject?
	{
		get { _sink }
		set
		{
			if _sink !== newValue
			{
				_sessionQueue.async {
					self._sink?.videoSession = nil
					self._sink = newValue
					self._sink?.videoSession = self._captureSession
				}
			}
		}
	}


	public init(source : RFVideoSourceObject)
	{
		self.source = source
		super.init()
		_setUpInputOutput()
		_addStateObservers()
	}

	deinit
	{
		print("in deinit of \(self)")
		_sink?.videoSession = nil
		_cancellableSubscriptions.forEach{ $0.cancel() }
		stop()
	}

	public func start()
	{
		_sessionQueue.async { [weak self] in
			guard let session = self?._captureSession,
				!session.isRunning && !session.inputs.isEmpty else { return }
			session.startRunning()
		}
	}

	public func pause()
	{
		stop()
	}

	public func stop()
	{
		_sessionQueue.async { [weak self] in
			self?._captureSession.stopRunning()
		}
	}

	@objc public dynamic var isRunning = false

	private func _setUpInputOutput()
	{
		_captureSession.beginConfiguration()
		defer{ _captureSession.commitConfiguration() }

		guard _captureSession.canAddInput(source.videoInput) else { return }
		_captureSession.addInput(source.videoInput)

		_observer = _captureSession.observe(\.isRunning, options: [.new]) { [unowned self] (model, change) in
			DispatchQueue.main.async { [weak self] in self?.isRunning = change.newValue! }
		}

		_videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "RFVideoStreamOutput", qos: .userInteractive, attributes: [], autoreleaseFrequency: .workItem))
		_videoOutput.alwaysDiscardsLateVideoFrames = true
		_videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA] as [String : Any]
		if _captureSession.canAddOutput(_videoOutput)
		{
			_captureSession.addOutput(_videoOutput)
		}
	}

	private func _applyFilters(_ pixelBuffer : CVPixelBuffer)
	{
		filters.forEach { filter in

		}
	}

	private func _addStateObservers()
	{
		let didEnterBgSubscription = NotificationCenter.default.publisher(for: UIApplication.didEnterBackgroundNotification).sink { _ in
			self.stop()
		}

		let willEnterFgSubscription = NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification).sink { _ in
			self.start()
		}

		let thermalStateSubscription = NotificationCenter.default.publisher(for: ProcessInfo.thermalStateDidChangeNotification).sink {
			let processInfo = $0.object as! ProcessInfo
			if (processInfo.thermalState == .serious) || (processInfo.thermalState == .critical)
			{
				self.stop()
			}
		}

		let runtimeErrorSubscription = NotificationCenter.default.publisher(for: NSNotification.Name.AVCaptureSessionRuntimeError).sink {
			guard let runtimeErrorMessage = $0.userInfo?[AVCaptureSessionErrorKey] else { return }
			debugPrint(runtimeErrorMessage)
		}

		let interruptionSubscription = NotificationCenter.default.publisher(for: NSNotification.Name.AVCaptureSessionWasInterrupted).sink { _ in
			debugPrint("Session is interrupted until the app becomes full-screen again.")
		}

		let interruptionEndSubscription = NotificationCenter.default.publisher(for: NSNotification.Name.AVCaptureSessionInterruptionEnded).sink { _ in
			debugPrint("Session continues")
		}

		let subjectAreaChangeSubscription = NotificationCenter.default.publisher(for: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange).sink { _ in
			debugPrint("The video area has changed.")
		}

		_cancellableSubscriptions.append(contentsOf:[didEnterBgSubscription, willEnterFgSubscription, thermalStateSubscription,
			runtimeErrorSubscription, interruptionSubscription, interruptionEndSubscription, subjectAreaChangeSubscription])

	}

}


extension RFVideoStream : AVCaptureVideoDataOutputSampleBufferDelegate
{
	public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection)
	{
		guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
		_applyFilters(frame)
		_sink?.process(frame)
	}
}
