//  Copyright 2020 Robo.Fish UG

import SwiftUI
import UIKit
import AVFoundation

public struct RFVideoView : UIViewRepresentable
{
	private weak var _stream : RFVideoStream?
	private let _videoView : RFVideoUIView

	public init(stream : RFVideoStream)
	{
		_stream = stream
		_videoView = RFVideoUIView(frame:.zero)
	}

	public func makeUIView(context: Context) -> UIView
	{
		_stream?.start()
	#if false
		let view = UIView(frame: .zero)
		if let subview = _stream?.sink.view!
		{
			subview.translatesAutoresizingMaskIntoConstraints = false
			view.addSubview(subview)
			view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[s]|", options: [], metrics: nil, views: ["s":subview]))
			view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[s]|", options: [], metrics: nil, views: ["s":subview]))
		}
		return view
	#else
		_stream?.sink = _videoView
		return _videoView
	#endif
	}

	public func updateUIView(_ uiView: UIView, context: Context)
	{
		uiView.setNeedsLayout()
	}
}



public class RFVideoUIView : UIView
{
	private let _videoPreviewLayer = AVCaptureVideoPreviewLayer()

	private var interfaceOrientation : AVCaptureVideoOrientation
	{
		let uiOrientation = self.window?.windowScene?.interfaceOrientation ?? .portrait
		return AVCaptureVideoOrientation(rawValue: uiOrientation.rawValue) ?? .portrait
	}

	override public init(frame: CGRect)
	{
		super.init(frame: frame)
		_initialize()
	}

	required public init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
		_initialize()
	}

	override public func layoutSubviews()
	{
		super.layoutSubviews()
		_adjustVideoPreviewLayerFrame()
	}

	private func _initialize()
	{
		_videoPreviewLayer.isHidden = false
		_videoPreviewLayer.videoGravity = .resizeAspectFill
		//layer.insertSublayer(_videoPreviewLayer, at: 0)
		layer.addSublayer(_videoPreviewLayer)
	}

	private func _adjustVideoPreviewLayerFrame()
	{
		_videoPreviewLayer.connection?.videoOrientation = interfaceOrientation
		_videoPreviewLayer.frame = bounds
	}
}

extension RFVideoUIView : RFVideoSink
{
	public func process(_: CVPixelBuffer) -> Bool
	{
		false
	}

	public var view: UIView?
	{
		self
	}

	public var videoSession : AVCaptureSession?
	{
		get { _videoPreviewLayer.session }
		set { if _videoPreviewLayer.session != newValue { _videoPreviewLayer.session = newValue } }
	}
}
