//  Copyright 2020 Robo.Fish UG

import Foundation

public protocol RFVideoFilter : RFVideoSource, RFVideoProcessor
{
}
